#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

my $all = [];

for my $path (@ARGV)
{
    next unless $path =~ /P[0-9]+\.htm/;
    next if $path =~ /P0.htm/;
    open HTML, '<'. $path;
    my $text = join '', (<HTML>);
    $text =~ s/[\n\r\l]//g;

    #fix some typos in the source text
    $text =~ s/CH\'LDREN\'S/CHILDREN\'S/g;
    $text =~ s/W\'NDOW/WINDOW/g;
    $text =~ s/>oncentrated/>concentrated/g;

    my $s;

    ($s->{title}, $s->{number}) = $text =~ m/<TABLE WIDTH=90%><TR><TD ALIGN=left><B><FONT SIZE=\+1>([^<]*)<TD ALIGN=right><B>([^<]*)<\/TABLE>/gs;

    ($s->{section}) = $text =~ m/<TABLE WIDTH=90%><TR><TD><B>([^>]*)<TD ALIGN=right>/;

    ($s->{problem}, $s->{solution}) = $text =~ m/<TD COLSPAN=2><I><B>Problem<\/B><\/I><BR>(.*?)<TR><TD COLSPAN=2><I><B>Solution<\/B><\/I><BR>(.*?)<TR>/;

    $s->{problem} =~ s/<BR>/\n/g;
    $s->{solution} =~ s/<BR>/\n/g;
    $s->{solution} =~ s/\xB7/./g;

    my ($higher) = $text =~ m/<TD WIDTH=50% VALIGN=top><B>Select High Order Pattern and <INPUT TYPE="button" VALUE=Go onClick="go\(links\.back\)"> to it\.<BR><SELECT NAME=back SIZE=4>(.*?)<\/SELECT>/gs; 

    $s->{higher} = [];
    unless ($text =~ m/There is no High Order Pattern to choose/)
    {
        push @{$s->{higher}}, {number => $1, title => $2} while $higher =~ /<OPTION VALUE="P([0-9]*).htm">(?:&nbsp;| |[0-9])*([^<\n\r\l]*)/g;
    }

    my ($lower) = $text =~ m/<TD WIDTH=50% VALIGN=top><B>Select Low Order Pattern and <INPUT TYPE="button" VALUE=Go onClick="go\(links\.fwrd\)"> to it\.<BR><SELECT NAME=fwrd SIZE=4>(.*?)<\/SELECT>/gs;

    $s->{lower} = [];
    unless ($text =~ m/There is no Low Order Pattern to choose/)
    {
        push @{$s->{lower}}, {number => $1, title => $2} while $lower =~ /<OPTION VALUE="P([0-9]*).htm">(?:&nbsp;| |[0-9])*([^<\n\r\l]*)/g;
    }

    $all->[$s->{number}] = $s;
}

say '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
say '<html xmlns="http://www.w3.org/1999/xhtml">';

say '<head><title>A Pattern Language - Quick Reference</title></head>';
say '<body><h1>A Pattern Language</h1>';
say '<div id="toc"><h2>Contents</h2>';

my $section = $all->[1]->{section};
say '<h3><a href="#'. name ($all->[1]->{section}) .'">'. $all->[1]->{section} .'</a></h3><ul>';
for my $pattern (@{$all})
{
    next unless $pattern;
    unless ($pattern->{section} eq $section)
    {
        say '</ul><h3><a href="#'. name ($pattern->{section}) .'">'. $pattern->{section} .'</a></h3><ul>';
        $section = $pattern->{section};
    }
    say '  <li><a href="#'. name ($pattern->{title})
       .'">'. $pattern->{number} .' '. $pattern->{title} . '</a></li>';
}

say '</ul></div>';

$section = $all->[1]->{section};
say '<h2><a href="#'. name ($all->[1]->{section}) .'" name="'. name ($all->[1]->{section}) .'">'. $all->[1]->{section} .'</a></h2>';
for my $pattern (@{$all})
{
    next unless $pattern;
    unless ($pattern->{section} eq $section)
    {
        say '<h2><a href="#'. name ($pattern->{section}) .'" name="'. name ($pattern->{section}) .'">'. $pattern->{section} .'</a></h2>';
        $section = $pattern->{section};
    }
    say '<div class="pattern" id="'. name ($pattern->{title}) .'">';
    say '<h3><a href="#'. name ($pattern->{title})
       .'">'. $pattern->{number} .' '. $pattern->{title} . '</a></h3>';

    my $problem = $pattern->{problem};
    $problem =~ s/\n/<\/p><p>/g;
    say '<p><strong>Problem:</strong> '. $problem .'</p>';

    my $solution = $pattern->{solution};
    $solution =~ s/\n/<\/p><p>/g;
    say '<p><strong>Solution:</strong> '. $solution .'</p>';

    my @higher = map {'<a href="#'. name ($_->{title}) .'">'. $_->{number} .' '.$_->{title} .'</a>'} @{$pattern->{higher}};
    if (scalar @higher)
    {
        say '<p><strong>Higher patterns:</strong> ';
        say join ', ', @higher;
        say '</p>';
    }

    my @lower = map {'<a href="#'. name ($_->{title}) .'">'. $_->{number} .' '.$_->{title} .'</a>'} @{$pattern->{lower}};
    if (scalar @lower)
    {
        say '<p><strong>Lower patterns:</strong> ';
        say join ', ', @lower;
        say '</p>';
    }
    say '</div>';
}

say '</body></html>';

sub name
{
    my $name = shift;
    $name =~ s/[^a-zA-Z -]//g;
    $name =~ s/ +/-/g;
    $name =~ s/[ *-.]*$//;
    return lc $name;
}

0;
