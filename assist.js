'use strict';

for (const patternDiv of document.querySelectorAll(".pattern")) {
  patternDiv.querySelector("h3").insertAdjacentHTML("beforeend",
    `<input type="checkbox" value=${patternDiv.id} onchange=onHeadingToggle(event)>`)
}

function onHeadingToggle(event) {
  const patternId = event.target.value;
  if (event.srcElement.checked) {
    subset.insertAdjacentHTML("beforeend",
      `<li>
        ${document.getElementById(patternId).querySelector("h3 > a").outerHTML}
        <input type="checkbox" value=${patternId} checked=true onchange="onLiToggle(event)">
      </li>`);
  } else {
    subset.querySelector(`input[value=${patternId}]`).parentNode.remove();
  }
}

function onLiToggle(event) {
  const patternId = event.target.value;
  document.querySelector(`.pattern input[value=${patternId}]`).checked = false;
  event.srcElement.parentNode.remove();
}

function subsetToggle(event) {
  if (event.srcElement.checked) {
    let includeHeading = false;
    for (let node = document.querySelector("main").lastElementChild; node.id !== "toc"; node = node.previousElementSibling) {
      if (node.tagName === "DIV") {
        if (node.querySelector("input:checked")) {
          node.style.display = null;
          includeHeading = true;
        } else {
          node.style.display = "none";
        }
      } else if (node.tagName === "H2") {
        if (includeHeading) {
          node.style.display = null;
          includeHeading = false;
        } else {
          node.style.display = "none";
        }
      }
    }
  } else {
    for (let node = document.querySelector("main").lastElementChild; node.id !== "toc"; node = node.previousElementSibling) {
      node.style.display = null;
    }
  }
}
