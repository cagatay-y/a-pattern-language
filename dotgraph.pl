#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

my $all = [];

for my $path (@ARGV)
{
    next unless $path =~ /P[0-9]+\.htm/;
    next if $path =~ /P0.htm/;
    open HTML, '<'. $path;
    my $text = join '', (<HTML>);
    $text =~ s/[\n\r\l]//g;

    my $s;

    ($s->{title}, $s->{number}) = $text =~ m/<TABLE WIDTH=90%><TR><TD ALIGN=left><B><FONT SIZE=\+1>([^<]*)<TD ALIGN=right><B>([^<]*)<\/TABLE>/gs;

    ($s->{section}) = $text =~ m/<TABLE WIDTH=90%><TR><TD><B>([^>]*)<TD ALIGN=right>/;

    ($s->{problem}, $s->{solution}) = $text =~ m/<TD COLSPAN=2><I><B>Problem<\/B><\/I><BR>(.*?)<TR><TD COLSPAN=2><I><B>Solution<\/B><\/I><BR>(.*?)<TR>/;

    $s->{problem} =~ s/<BR>/\n/g;
    $s->{solution} =~ s/<BR>/\n/g;
    $s->{solution} =~ s/\xB7/./g;

    my ($higher) = $text =~ m/<TD WIDTH=50% VALIGN=top><B>Select High Order Pattern and <INPUT TYPE="button" VALUE=Go onClick="go\(links\.back\)"> to it\.<BR><SELECT NAME=back SIZE=4>(.*?)<\/SELECT>/gs; 

    $s->{higher} = [];
    unless ($text =~ m/There is no High Order Pattern to choose/)
    {
        push @{$s->{higher}}, {number => $1, title => $2} while $higher =~ /<OPTION VALUE="P([0-9]*).htm">(?:&nbsp;| |[0-9])*([^<\n\r\l]*)/g;
    }

    my ($lower) = $text =~ m/<TD WIDTH=50% VALIGN=top><B>Select Low Order Pattern and <INPUT TYPE="button" VALUE=Go onClick="go\(links\.fwrd\)"> to it\.<BR><SELECT NAME=fwrd SIZE=4>(.*?)<\/SELECT>/gs;

    $s->{lower} = [];
    unless ($text =~ m/There is no Low Order Pattern to choose/)
    {
        push @{$s->{lower}}, {number => $1, title => $2} while $lower =~ /<OPTION VALUE="P([0-9]*).htm">(?:&nbsp;| |[0-9])*([^<\n\r\l]*)/g;
    }

    $all->[$s->{number}] = $s;
}

say 'digraph patterns {';
say 'graph [splines=true, overlap=false];';
for my $pattern (@{$all})
{
    next unless $pattern->{number} and $pattern->{title};
    my $pos_vert = 11000 - (40 * $pattern->{number});
    say $pattern->{number} .' [label="'. $pattern->{number} .' '. $pattern->{title} .'", pos="0.0,'. $pos_vert .'"];';
    #for my $link (@{$pattern->{higher}}, @{$pattern->{lower}})
    for my $link (@{$pattern->{lower}})
    {
        my $weight = 256 - abs ($pattern->{number} - $link->{number});
        say $pattern->{number} .' -> '. $link->{number} .' [weight="'. $weight .'"];';
    }
}
say '}';

0;
